# Text Labelling System 

This is the Implementation of the automated text labelling phase for manuscripts. The complete Implementation of this Project including the recognition phase of the labelled outputs has been documented as a publication and has been **Presented** at the **International Conference on Data Science,Machine Learning and Application(ICDSMLA 2020)**. Currently, **In Press - Springer**.


