import tkinter as tk
from PIL import Image,ImageTk
from imutils.object_detection import non_max_suppression
import numpy as np
import argparse
import time
import cv2
import math
import matplotlib
matplotlib.use("Qt5Agg")
import matplotlib.pyplot as plt
from tkinter import ALL, EventType

class ExampleApp(tk.Tk):

    def __init__(self):
        tk.Tk.__init__(self)

        self.newly_added_count=0
        self.x = 0
        self.y = 0

        self.img = ImageTk.PhotoImage(Image.open(r"/home/sujith/cns_lab/handwrp/east/EAST/tkinter/iam.png"))
        self.canvas = tk.Canvas(self, width=1080, height=720) #checkout max size - bigger photos than canvas doesn't fit
        #self.canvas.pack()

        self.canvas.bind("<ButtonPress-1>", self.on_button_press) #left clicking
        self.canvas.bind("<B1-Motion>", self.on_move_press) #left click and drag around
        self.canvas.bind("<Motion>", self.moving)
        self.canvas.bind("<B3-Motion>", self.bounding_box_resize) #right clicking mouse and drag


        self.canvas_image = self.canvas.create_image(10, 10, anchor='nw', image=self.img) #nw : north west

        ##Newly added for the resizing image to canvas size
        self.canvas.pack(fill=tk.BOTH, expand=1)
        self.canvas.bind("<Configure>", self.resize)

        self.start_x = None
        self.start_y = None
        self.rect = None

    ##Newly added code to resize image to canvas size
    def resize(self, event):
        ##new_count not relevant to this function, but since this gets called first, good place to initialize global variable
        global new_count
        self.new_count=count
        img = Image.open(r"/home/sujith/cns_lab/handwrp/east/EAST/tkinter/iam.png").resize((event.width, event.height), Image.ANTIALIAS)
        self.img = ImageTk.PhotoImage(img)
        self.canvas.itemconfig(self.canvas_image , image=self.img)

    ##Newly added for zoom
    def do_zoom(event):
        factor = 1.001 ** event.delta
        canvas.scale(ALL, event.x, event.y, factor, factor)


    def right_click(self, event):
        self.canvas.delete(self.canvas.find_closest(event.x, event.y))

    ##Modified function to remove overlapping bounding box problem :
    def moving(self, event):
        all_rect=[]
        for i in range(1,count+1):
            all_rect.append(self.canvas.find_withtag('rect'+str(i)))

        #print("Count here is"+str(count))
        #print("new count here is"+str(self.new_count))
        
        #Newly added :Identifying the new bounding boxes which have been drawn
        if(self.new_count>count):
            #print("Check check")
            for i in range(count+1,self.new_count+1):
                #print(i)
                all_rect.append(self.canvas.find_withtag('rect'+str(i)))
        #End of new added code

        x = event.x
        y = event.y

        for i in all_rect:
            x1, y1, x2, y2 = self.canvas.bbox(i)
            if x1 <= x <= x2 and y1 <= y <= y2:
                self.canvas.itemconfig(i, width=5.0, outline='yellow')
            else:
                self.canvas.itemconfig(i, width=5.0, outline='red')

    def on_button_press(self, event):
        self.start_x = event.x
        self.start_y = event.y
        ##Added a new variable to make tags for new rectangles
        self.new_count=self.new_count+1
        self.rect = self.canvas.create_rectangle(self.x, self.y, 0, 0, width=2.0, outline='red', tag=('rect'+str(self.new_count)))
        print("test number ")
    
    def on_move_press(self, event):
        curX, curY = (event.x, event.y)
        self.canvas.coords(self.rect, self.start_x, self.start_y, curX, curY)

    def on_button_release(self, event):
        pass
    
    #resizing problem is in this function ##DEBUG THIS : Done
    #resizing on every corner
    #resizing must be done according to how we resize in ppts
        
##########New code inserted for ppt type resizing#################################################################################
        x1,y1,x2,y2=self.canvas.coords(i)
        new_x1=x1
        new_x2=x2
        new_y1=y1
        new_y2=y2

        #For upward and downward elongation :
        
        if(event.x>new_x1+10 and event.x<new_x2-10):
            #For upper line of rectangle :
           
            if(event.y<new_y2-5 and ((event.y>new_y1-20) and (event.y<new_y1+20))):
                self.canvas.config(cursor="sb_v_double_arrow")
                self.canvas.coords(i,(new_x1,event.y,new_x2,new_y2))
                new_y1=event.y
            
            #For lower line of rectangle :
            
            elif(event.y>new_y1+5 and (event.y>new_y2-20) and event.y<new_y2+20):
                self.canvas.config(cursor="sb_v_double_arrow")
                self.canvas.coords(i,(new_x1,new_y1,new_x2,event.y))
                new_y2=event.y
        
        #For elongation of left side of rectangle :
        
        elif(event.x<new_x2 and ((event.y>new_y1+10) and (event.y<new_y2-10)) and ((event.x>new_x1-20) and (event.x<new_x1+20))):
           self.canvas.config(cursor="sb_h_double_arrow")
           self.canvas.coords(i,(event.x,new_y1,new_x2,new_y2))
           new_x1=event.x
        
        #For elongation of right side of the rectangle :
        
        elif(event.x>new_x1 and ((event.y>new_y1+10) and (event.y<new_y2-10)) and ((event.x>new_x2-20) and (event.x<new_x2+20))):
           self.canvas.config(cursor="sb_h_double_arrow")
           self.canvas.coords(i,(x1,y1,event.x,y2))
           new_x2=event.x 
        
        #For Diagonal Resizing :
        
        #For left diagonal resizing :
        
        elif(event.x<=new_x1+10 and event.x>=new_x1-10 and ((event.y<=new_y1+10) and (event.y>=new_y1-10))):
            self.canvas.config(cursor="top_left_corner")
            self.canvas.coords(i,(event.x,event.y,new_x2,new_y2))
            new_x1=event.x
            new_y1=event.y
            

        elif(event.x<=new_x1+20 and event.x>=new_x1-10 and ((event.y<=new_y2+10) and (event.y>=new_y2-10))):
            self.canvas.config(cursor="bottom_left_corner")
            self.canvas.coords(i,(event.x,new_y1,new_x2,event.y))
            new_x1=event.x
            new_y2=event.y
            

        
        #For right diagonal resizing :
        
        elif(event.x>=new_x2-20 and event.x<=new_x2+20 and((event.y<=new_y1+10) and (event.y>=new_y1-20))):
            self.canvas.config(cursor="top_right_corner")
            self.canvas.coords(i,(new_x1,event.y,event.x,new_y2))
            new_x2=event.x
            new_y1=event.y
            

        elif(event.x>=new_x2-20 and event.x<=new_x2+20 and ((event.y<=new_y2+20) and (event.y>=new_y2-10))):
            self.canvas.config(cursor="bottom_right_corner")
            self.canvas.coords(i,(new_x1,new_y1,event.x,event.y))
            new_x2=event.x
            new_y2=event.y



        
###########End of new code inserted for ppt type resizing#########################################################################
    
#####New code inserted to fix overlapping boxes problem#############
    
    ##this function Identifies the object of interest to resize and sends it to the callback function to resize
    def bounding_box_resize(self,event):
        print(count)
        i=self.canvas.find_withtag("current") #Choose only the currently selected rectangle
        bbox= self.canvas.bbox(i) # Get the bounding box coordinates of the rectangle
        #print("check1")
        if bbox[0] <= event.x and bbox[2] >= event.x and bbox[1] <= event.y and bbox[3] >= event.y:#check if the mouse click is on the bounding box lines
            #print("True")
            this_rect=i
            self.callback(event,this_rect) 
            
        #else:
           

#####End of new code inserted for overlapping boxes problem######


    def keypress(self,event):
        all_rects=[]
        for k in range(1,count+1):
            all_rects.append(self.canvas.find_withtag('rect'+str(k)))

        #print("Count here is"+str(count))
        #print("new count here is"+str(self.new_count))
        
        #Newly added :Identifying the new bounding boxes which have been drawn
        if(self.new_count>count):
            #print("Check check")
            for l in range(count+1,self.new_count+1):
                #print(i)
                all_rects.append(self.canvas.find_withtag('rect'+str(l)))
        #End of new added code

        x = event.x
        y = event.y

        for m in all_rects:
            x1, y1, x2, y2 = self.canvas.bbox(m)
            if x1 <= x <= x2 and y1 <= y <= y2:
                x2=0
                y2=0
                if event.char == "a": x2 = -10 # left 
                elif event.char == "d": x2 = 10 # right
                elif event.char == "w": y2 = -10 # up
                elif event.char == "s": y2 = 10 # down
                elif event.char == "r": self.canvas.delete(m)
                self.canvas.move(m, x2, y2)  
            else:
                pass

    
    def do_zoom(self,event):
        factor = 1.001 ** event.delta
        self.canvas.scale(tk.ALL, event.x, event.y, factor, factor)


if __name__ == "__main__":
    called=False
    app = ExampleApp()
    app.bind("<Key>", app.keypress)
    # construct the argument parser and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--image", type=str,
        help="path to input image")
    ap.add_argument("-east", "--east", type=str,
        help="path to input EAST text detector")
    ap.add_argument("-c", "--min-confidence", type=float, default=0.5,
        help="minimum probability required to inspect a region")
    ap.add_argument("-w", "--width", type=int, default=320,
        help="resized image width (should be multiple of 32)")
    ap.add_argument("-e", "--height", type=int, default=320,
        help="resized image height (should be multiple of 32)")
    args = vars(ap.parse_args())

    # load the input image and grab the image dimensions
    image = cv2.imread(args["image"])
    
    ##Newly added for resizing of image to fit canvas :
    image = cv2.resize(image, (1080,720))
    orig = image.copy()
    (H, W) = image.shape[:2]

    # set the new width and height and then determine the ratio in change
    # for both the width and height
    (newW, newH) = (args["width"], args["height"])
    rW = W / float(newW)
    rH = H / float(newH)

    # resize the image and grab the new image dimensions
    image = cv2.resize(image, (newW, newH))
    (H, W) = image.shape[:2]

    # define the two output layer names for the EAST detector model that
    # we are interested -- the first is the output probabilities and the
    # second can be used to derive the bounding box coordinates of text
    layerNames = [
        "feature_fusion/Conv_7/Sigmoid",
        "feature_fusion/concat_3"]

    # load the pre-trained EAST text detector
    print("[INFO] loading EAST text detector...")
    net = cv2.dnn.readNet(args["east"])

    # construct a blob from the image and then perform a forward pass of
    # the model to obtain the two output layer sets
    blob = cv2.dnn.blobFromImage(image, 1.0, (W, H),
        (123.68, 116.78, 103.94), swapRB=True, crop=False)
    start = time.time()
    net.setInput(blob)
    (scores, geometry) = net.forward(layerNames)
    end = time.time()

    # show timing information on text prediction
    print("[INFO] text detection took {:.6f} seconds".format(end - start))

    # grab the number of rows and columns from the scores volume, then
    # initialize our set of bounding box rectangles and corresponding
    # confidence scores
    (numRows, numCols) = scores.shape[2:4]
    rects = []
    confidences = []

    # loop over the number of rows
    for y in range(0, numRows):
        # extract the scores (probabilities), followed by the geometrical
        # data used to derive potential bounding box coordinates that
        # surround text
        scoresData = scores[0, 0, y]
        xData0 = geometry[0, 0, y]
        xData1 = geometry[0, 1, y]
        xData2 = geometry[0, 2, y]
        xData3 = geometry[0, 3, y]
        anglesData = geometry[0, 4, y]

        # loop over the number of columns
        for x in range(0, numCols):
            # if our score does not have sufficient probability, ignore it
            if scoresData[x] < args["min_confidence"]:
                continue

            # compute the offset factor as our resulting feature maps will
            # be 4x smaller than the input image
            (offsetX, offsetY) = (x * 4.0, y * 4.0)

            # extract the rotation angle for the prediction and then
            # compute the sin and cosine
            angle = anglesData[x]
            cos = np.cos(angle)
            sin = np.sin(angle)

            # use the geometry volume to derive the width and height of
            # the bounding box
            h = xData0[x] + xData2[x]
            w = xData1[x] + xData3[x]

            # compute both the starting and ending (x, y)-coordinates for
            # the text prediction bounding box
            endX = int(offsetX + (cos * xData1[x]) + (sin * xData2[x]))
            endY = int(offsetY - (sin * xData1[x]) + (cos * xData2[x]))
            startX = int(endX - w)
            startY = int(endY - h)

            # add the bounding box coordinates and probability score to
            # our respective lists
            rects.append((startX, startY, endX, endY))
            confidences.append(scoresData[x])

    # apply non-maxima suppression to suppress weak, overlapping bounding
    # boxes
    boxes = non_max_suppression(np.array(rects), probs=confidences)
    print(called)
    # loop over the bounding boxes
    
    # newly added : added a new variable count and modified the tags from just rect, to rect plus count, where count keeps getting incremented as rectangles are drawn
    count=0 
    turned=False
    if called:
        pass
    else:
        called=True
        for (startX, startY, endX, endY) in boxes:
            startX = int(startX * rW)
            startY = int(startY * rH)
            endX = int(endX * rW)
            endY = int(endY * rH)
            count=count+1
            if (count>1):
                prev_rect=app.canvas.find_withtag('rect'+str(count-1))
                x0,y0,x1,y1=app.canvas.coords(prev_rect)
                turned=True             
            else:
                pass
            app.canvas.create_rectangle(startX+10, startY+10, endX+10, endY+10, width=2.0, outline='red', tag=('rect'+str(count)))
            app.canvas.create_line((endX+10),(endY-15),(endX+40),(endY-15),fill="green",width=5.0, arrow=tk.LAST)
    app.mainloop()
"""

            if ((event.x > x1 or event.x > x2) and (event.y > y1 or event.y > y2)) :
                self.canvas.coords(i, (x1, y1,event.x,event.y))
                break
            else:
                pass
"""